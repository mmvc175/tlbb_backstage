# -*- coding: UTF-8 -*-
from flask import Flask, render_template, request, redirect, session

from performance import *

# 网页内容
my_website = myconf.config['my_website']

app = Flask(__name__)
app.secret_key = 'AjJSHjdknCMDhuinmsnhbbhSJHDBKDJBFddjvfvjlakjeQ1829nnf829js497hD4320WEer56'


# 功能前台页面
@app.route('/')
def index():
    return render_template('index.html', my_website=my_website)

#登录、注销功能
@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'GET':
        if not session.get('usr'):
            return render_template('login.html')
        else:
            return redirect('/gmIndex')
    if request.form.get('Name') == myconf.config['gm_usrname'] and request.form.get('Password') == myconf.config[
        'gm_tool_pwd']:
        session['usr'] = request.form['Name']
        return redirect('/gmIndex')
    else:
        return '用户名或密码错误！！！！'

@app.route('/logout')
def logout():
    session.clear()
    return render_template('login.html')


@app.route('/gmIndex')
def gmIndex():
    if not session.get('usr'):
        return redirect('/login')
    try:
        conn_web = pymysql.connect(user='root', password=myconf.config['DB_PASSWORD'], host=myconf.config['IP'],
                                   database='web', port=myconf.config['DB_PORT'])
        c_web = conn_web.cursor(pymysql.cursors.DictCursor)
    except:
        return '配置文件的数据库信息填写错误！！请检查您的数据库IP、密码和端口是否填写正确，然后重新执行run.sh脚本！！'
    sql_web = 'select * from account'
    c_web.execute(sql_web)
    data_info = c_web.fetchall()

    c_web.close()
    conn_web.close()
    return render_template('gmindex.html', data_info=data_info)


@app.route('/gm_accountBlock')
def gm_accountBlock():
    if not session.get('usr'):
        return redirect('/login')
    block = request.args.get('block')
    unblock = request.args.get('unblock')
    clearpoint = request.args.get('clearpoint')
    try:
        conn_web = pymysql.connect(user='root', password=myconf.config['DB_PASSWORD'], host=myconf.config['IP'],
                                   database='web', port=myconf.config['DB_PORT'])
        c_web = conn_web.cursor(pymysql.cursors.DictCursor)
    except:
        return '''

        配置文件的数据库信息填写错误！！请检查您的数据库IP、密码和端口是否填写正确，然后重新执行run.sh脚本！！      '''
    if block:
        sql_block = '''
        update account set id_card=1 where name=%s
        ''' % ("'" + block + "'")
        try:
            c_web.execute(sql_block)
            conn_web.commit()
            c_web.close()
            conn_web.close()
            return redirect('/gmIndex')
        except:
            conn_web.rollback()
            c_web.close()
            conn_web.close()
            return redirect('/gmIndex')
    if unblock:
        sql_unblock = '''
        update account set id_card=NULL where name=%s
        ''' % ("'" + unblock + "'")
        try:
            c_web.execute(sql_unblock)
            conn_web.commit()
            c_web.close()
            conn_web.close()
            return redirect('/gmIndex')
        except:
            conn_web.rollback()
            c_web.close()
            conn_web.close()
            return redirect('/gmIndex')
    if clearpoint:
        sql_clearponit = 'update account set point=0 where name=%s' % ("'" + clearpoint + "'")
        try:
            c_web.execute(sql_clearponit)
            conn_web.commit()
            c_web.close()
            conn_web.close()
            return redirect('/gmIndex')
        except:
            conn_web.rollback()
            c_web.close()
            conn_web.close()
            return redirect('/gmIndex')


@app.route('/gm_char')
def gm_char():
    if not session.get('usr'):
        return redirect('/login')

    try:
        conn = pymysql.connect(user='root', password=myconf.config['DB_PASSWORD'], host=myconf.config['IP'],
                               database='tlbbdb', port=myconf.config['DB_PORT'])
        c = conn.cursor(pymysql.cursors.DictCursor)
    except:
        return '配置文件的数据库信息填写错误！！请检查您的数据库IP、密码和端口是否填写正确，然后重新执行run.sh脚本！！'
    sql = '''
    select accname,charguid,yuanbao,level,menpai,zengdian from t_char
    '''

    c.execute(sql)
    data_info = c.fetchall()
    c.close()
    conn.close()
    return render_template('gm_char.html', data_info=data_info)


@app.route('/gm_chartool')
def gm_chartool():
    if not session.get('usr'):
        return redirect('/login')
    try:
        conn = pymysql.connect(user='root', password=myconf.config['DB_PASSWORD'], host=myconf.config['IP'],
                               database='tlbbdb', port=myconf.config['DB_PORT'])
        c = conn.cursor(pymysql.cursors.DictCursor)
    except:
        return '配置文件的数据库信息填写错误！！请检查您的数据库IP、密码和端口是否填写正确，然后重新执行run.sh脚本！！'
    block_char = request.args.get('block_char')
    # clearbag_char =request.args.get('clearbag_char')
    unblock_char = request.args.get('unblock_char ')
    del_char = request.args.get('del_char')
    clearyb_char = request.args.get('unblock_char')
    if block_char:
        sql_block_char = '''
            update t_char set settings='00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000' where charguid=%s;
        ''' % block_char
        try:
            c.execute(sql_block_char)
            conn.commit()
            c.close()
            conn.close()
            return redirect('/gm_char')
        except:
            conn.rollback()
            c.close()
            conn.close()
            return redirect('/gm_char')
    if unblock_char:
        try:
            sql_unblock_char = '''
                update t_char set settings='0080F5200000040000000173010000017D01000001810100000000000000000000000000000000000116000000012300000002010000000101000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001000000FF0000000000000000000000000000D233000000000000000000000000000128B3420E0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000' where charguid=%s;
            ''' % unblock_char
            c.execute(sql_unblock_char)
            conn.commit()
            c.close()
            conn.close()
            return redirect('/gm_char')
        except:
            conn.rollback()
            c.close()
            conn.close()
            return redirect('/gm_char')
    if del_char:
        try:
            sql_del = '''
            delete from t_char where charguid=%s;
            ''' % del_char

            c.execute(sql_del)
            conn.commit()
            c.close()
            conn.close()
            return redirect('/gm_char')
        except:
            conn.rollback()
            c.close()
            conn.close()
            return redirect('/gm_char')
    # if clearbag_char:
    #     pass
    if clearyb_char:
        try:
            sql_clearyb = '''
            update t_char set yuanbao=0,zengdian=0 where charguid=%s;
            ''' % clearyb_char
            c.execute(sql_clearyb)
            conn.commit()
            c.close()
            conn.close()
            return redirect('/gm_char')
        except:
            conn.rollback()
            c.close()
            conn.close()
            return redirect('/gm_char')


@app.route('/register')
def register():
    get_code = Id_code().x
    return render_template('register.html', get_code=get_code, my_website=my_website)


@app.route('/mpwd')
def mpwd():
    get_code = Id_code().x
    return render_template('mpwd.html', get_code=get_code, my_website=my_website)


@app.route('/mapsave')
def map_save():
    get_code = Id_code().x
    return render_template('mapsave.html', get_code=get_code, my_website=my_website)


@app.route('/unblock')
def account_unblock():
    get_code = Id_code().x
    return render_template('unblock.html', get_code=get_code, my_website=my_website)


@app.route('/gmpoint')
def gmpoint():
    get_code = Id_code().x
    return render_template('gmpoint.html', get_code=get_code, my_website=my_website)


# 功能实现函数
@app.route('/register_result', methods=['POST', 'GET'])
def register_result():
    if request.method == 'POST':
        result = request.form
        bg = Backstage()
        if result['password'] != result['re_password']:
            return '两次输入的密码不一致'
        usr = result['usr']
        passwd = result['password']
        q = result['superpwd']
        or_code = result['or_code']
        code_catch = result['usr_input']
        if or_code != code_catch:
            return render_template('result.html', response='验证码输入错误')
        response = bg.register(usr, passwd, q)
        return render_template('result.html', result=result, response=response)
    else:
        return redirect('/register')


@app.route('/mpwd_result', methods=["POST", 'GET'])
def mpwd_result():
    if request.method == 'POST':
        result = request.form
        or_code = result['or_code']
        code_catch = result['usr_input']
        if or_code != code_catch:
            return render_template('result.html', response='验证码输入错误')
        if result['password'] != result['re_password']:
            response = '两次输入的新密码不一致！！！！！'
            return render_template('result.html', response=response)
        usr = result['usr']
        question_demo = result['superpwd']
        new_passwd = result['password']
        bg = Backstage()
        response = bg.modify_passwd(usr=usr, question_demo=question_demo, new_passwd=new_passwd)
        return render_template('result.html', response=response)
    else:
        return redirect('/mpwd')


@app.route('/mapsave_result', methods=['POST', 'GET'])
def mapsave_result():
    if request.method == 'POST':
        result = request.form
        or_code = result['or_code']
        code_catch = result['usr_input']
        if result['password'] != result['re_password']:
            return render_template('result.html', response='两次密码输入不一致！')
        if or_code != code_catch:
            return render_template('result.html', response='验证码输入错误！')
        usr = result['usr']
        password = result['password']
        bg = Backstage()
        response = bg.char_save(usr=usr, passwd=password)
        return render_template('result.html', response=response)
    else:
        return redirect('/map_save')


@app.route('/unblock_result', methods=["POST", "GET"])
def unblock_result():
    if request.method == 'POST':
        result = request.form
        or_code = result['or_code']
        code_catch = result['usr_input']
        if result['password'] != result['re_password']:
            return render_template('result.html', response='两次输入的密码不一致！')
        if or_code != code_catch:
            return render_template('result.html', response='验证码输入错误！')
        usr = result['usr']
        password = result['password']
        bg = Backstage()
        response = bg.block_over(usr=usr, password=password)
        return render_template('result.html', response=response)
    else:
        return redirect('account_unblock')

#结果页待优化…… 优化内容 增加弹窗+改变内容分布格式
@app.route('/gmpoint_result', methods=["POST", "GET"])
def gmpoint_result():
    if request.method == 'POST':
        result = request.form
        or_code = result['or_code']
        code_catch = result['usr_input']
        if or_code != code_catch:
            return render_template('result.html', response='验证码输入错误！')
        password = result['password']
        conf_passwd = myconf.config['gm_tool_pwd']
        if password != conf_passwd:
            return render_template('result.html', response='Gm识别码错误！！！！非GM无法适用本功能！！！')
        usr = result['usr']
        point = result['gmpoints']
        bg = Backstage()
        response = bg.gm_point(usr, point)
        return render_template('result.html', response=response)
    else:
        return redirect('gmpoint')


if __name__ == '__main__':
    app.run()
