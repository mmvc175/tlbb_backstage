#!/bin/bash
#Author : Ataier QQ3284809656
#安装Docker

function docker_install() {
  echo "第1次检测：检查Docker......"
  docker -v
  if [ $? -eq 0 ]; then
    echo "检查到Docker已安装!"
  else
    echo "安装docker环境..."
    curl -sSL https://get.daocloud.io/docker | sh
    echo "安装docker环境...安装完成!"
  fi
  docker -v
  if [ $? -eq 0 ]; then
    echo "第2次检测：docker安装成功!"
  else
    sudo yum remove docker \
                docker-client \
                docker-client-latest \
                docker-common \
                docker-latest \
                docker-latest-logrotate \
                docker-logrotate \
                docker-engine
    sudo yum install -y yum-utils
    sudo yum-config-manager \
        --add-repo \
        https://download.docker.com/linux/centos/docker-ce.repo
    sudo yum install docker-ce docker-ce-cli containerd.io
    sudo systemctl start docker
  fi
  if [ $? -eq 0 ]; then
    echo "第3次检测：docker安装成功!"
  else
    echo '第3次检测：docker安装失败！请联系开发者邮箱截图您的报错3284809656@qq.com'
    exit;
  fi
  sed -i '$a\nameserver  8.8.8.8' /etc/resolv.conf
  sed -i '$a\nameserver  114.114.114.114' /etc/resolv.conf
  echo 'DOCKER_OPTS="--dns 8.8.8.8"' >/etc/default/docker
  # 创建公用网络==bridge模式
  #docker network create share_network
}

#Docker Build And Run
function docker_run() {

  docker stop "tl_backstage"
  docker rm -f "tl_backstage"
  docker rmi -f "tlbb_backstage"
  docker build -t "tlbb_backstage" .
  docker run -d -p 8881:8881 --name "tl_backstage" "tlbb_backstage"
  echo " HTTP://你的IP:8881 即可访问！！！"
  #read -p '是否隐藏8881端口，将网址变为http://您的IP/backstage ？ （ y / n ）' hiddenport

}


# 执行函数
docker_install
sudo systemctl restart docker
docker_run
