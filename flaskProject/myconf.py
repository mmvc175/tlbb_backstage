config = {
    'IP': '在这里输入您的数据库IP',  # 数据库所在IP，一般是Linux的IP地址
    'DB_PASSWORD': '这里是您的数据库密码',  # 数据库密码，不是Linux密码
    'DB_PORT': 3306,  # 数据库端口默认3306，如果您的不是3306，请自行更改
    'register': True,  # 网页自助注册功能是否开启，默认开启，False关闭
    'modify_passwd': True,  # 修改密码功能是否开启，默认开启，False关闭
    'gm_point': True,  # GM在线发放点数功能，默认开始，False关闭
    'block_over': True,  # 是否开启 --自助解封--功能，如果开启，GM的封号特权就将被架空！！！谨慎选择
    'gm_usrname':'zkwd888',  #GM登录的用户名，在账号管理时可能会用到！！可以自定
    'gm_tool_pwd': '1TeStMySeRvErGmToOl.29',  # 可自定，GM在线功能的密码，如果上面的功能未开启，可以不进行修改
    'my_website': 'https://www.zkwd888.ltd'  # 自己有主页的话写自己的，没有的话就随意填写，可不修改

}
